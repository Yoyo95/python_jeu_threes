﻿import sys
sys.path.append("../tiles")
import tiles_acces
import tiles_moves
sys.path.append("../life_cycle")
import cycle_game
from random import randint

def test_get_next_area_tiles():
    p = cycle_game.init_play()
    t = tiles_moves.get_area_next_tiles(p, 'init', None)
    tiles_moves.put_next_tiles(p, t)
    t = tiles_moves.get_area_next_tiles(p, 'encours', 1)
    tiles_moves.put_next_tiles(p, t)
    assert tiles_acces.get_nb_empty_rooms(p) == 13
    print("Test get_next_area_tiles = Ok")

def test_put_next_tiles():
    p = cycle_game.init_play()
    t = tiles_moves.get_area_next_tiles(p, 'init', None)
    tiles_moves.put_next_tiles(p, t)
    t = tiles_moves.get_area_next_tiles(p, 'encours', 3)
    tiles_moves.put_next_tiles(p, t)
    t = tiles_moves.get_area_next_tiles(p, 'encours', 2)
    tiles_moves.put_next_tiles(p, t)
    t = tiles_moves.get_area_next_tiles(p, 'encours', 3)
    tiles_moves.put_next_tiles(p, t)
    t = tiles_moves.get_area_next_tiles(p, 'encours', 1)
    tiles_moves.put_next_tiles(p, t)
    assert tiles_acces.get_nb_empty_rooms(p) == 10
    print('Test put_next_tiles = Ok')

def test_line_pack():
    p = {
        'n': 4,
        'nb_cases_libres': 7,
        'tiles': [1,2,0,3,0,6,0,3,0,3,2,3,0,3,0,0]
    }
    tiles_moves.line_pack(p, 0, 3, 0)
    tiles_moves.line_pack(p, 1, 0, 1)
    tiles_moves.line_pack(p, 2, 3, 0)
    tiles_moves.line_pack(p, 3, 0, 1)
    assert p['tiles'] == [0,1,2,3,6,0,3,0,0,3,2,3,3,0,0,0]
    print("Test fonction line_pack = Ok")

def test_column_pack():
    p = {
        'n': 4,
        'nb_cases_libres': 7,
        'tiles': [1,2,0,3,2,6,0,3,0,3,2,3,0,3,0,0]
    }
    tiles_moves.column_pack(p, 0, 3, 0)
    tiles_moves.column_pack(p, 1, 0, 1)
    tiles_moves.column_pack(p, 2, 3, 0)
    tiles_moves.column_pack(p, 3, 0, 1)
    assert p['tiles'] == [0,2,0,3,1,6,0,3,2,3,0,3,0,3,2,0]
    print("Test fonction column_pack = Ok")

def test_line_move():
    p = {
        'n': 4,
        'nb_cases_libres': 7,
        'tiles': [1,2,0,3,0,6,0,3,0,3,2,3,0,3,0,0]
    }
    tiles_moves.line_move(p, 0 , 0)
    tiles_moves.line_move(p, 1 , 1)
    tiles_moves.line_move(p, 2 , 0)
    tiles_moves.line_move(p, 3 , 1)
    assert p['tiles'] == [0,1,2,3,6,0,3,0,0,3,2,3,3,0,0,0]
    print("Test fonction line_move = Ok")

def test_column_move():
    p = {
        'n': 4,
        'nb_cases_libres': 6,
        'tiles': [0,2,0,3,
                  0,6,0,3,
                  1,3,2,3,
                  2,3,0,0]
    }
    tiles_moves.column_move(p, 0, 0)
    tiles_moves.column_move(p, 1, 1)
    tiles_moves.column_move(p, 2, 0)
    tiles_moves.column_move(p, 3, 1)
    assert p['tiles'] == [0,2,0,6,
                          0,6,0,3,
                          0,6,0,0,
                          3,0,2,0]
    print("Test fonction column_move = Ok")

def test_lines_move():
    p = {
        'n': 4,
        'nb_cases_libres': 6,
        'tiles': [0,2,0,3,
                  0,6,0,3,
                  1,3,2,3,
                  2,3,0,0]
    }
    tiles_moves.lines_move(p, 0)
    assert p['tiles'] == [0,0,2,3,0,0,6,3,1,3,2,3,0,2,3,0]
    p = {
        'n': 4,
        'nb_cases_libres': 6,
        'tiles': [0,2,0,3,
                  0,6,0,3,
                  1,3,2,3,
                  2,3,0,0]
    }
    tiles_moves.lines_move(p, 1)
    assert p['tiles'] == [2,0,3,0,6,0,3,0,1,3,2,3,2,3,0,0]
    print("Fonction lines_move = Ok")

def test_columns_move():
    p = {
        'n': 4,
        'nb_cases_libres': 6,
        'tiles': [0,2,0,3,
                  0,6,0,3,
                  1,3,2,3,
                  2,3,0,0]
    }
    tiles_moves.columns_move(p, 0)
    assert p['tiles'] == [0,0,0,0,0,2,0,0,0,6,0,6,3,6,2,3]
    p = {
        'n': 4,
        'nb_cases_libres': 6,
        'tiles': [0,2,0,3,
                  0,6,0,3,
                  1,3,2,3,
                  2,3,0,0]
    }
    tiles_moves.columns_move(p, 1)
    assert p['tiles'] == [0,2,0,6,1,6,2,3,2,6,0,0,0,0,0,0]
    print('Fonction columns_move = Ok')






