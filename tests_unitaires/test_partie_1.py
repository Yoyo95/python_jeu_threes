﻿import sys
sys.path.append("../tiles")
from tiles_acces import check_indice, check_room, get_value, set_value, is_room_empty
from tiles_moves import get_nb_empty_rooms
sys.path.append("../life_cycle")
from cycle_game import init_play, is_game_over, get_score

def test_init_play():
    p = init_play()
    assert p == {
        'n': 4,
        'nb_cases_libres': 16,
        'tiles': [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    }
    print("Fonction init_play = Validée.")

def test_check_indice():
    p = init_play()
    assert check_indice(p, 0)
    assert not check_indice(p, -1)
    assert check_indice(p, 2)
    assert not check_indice(p, 5)
    assert check_indice(p, 3)
    print("Fonction check_indice = Validée")

def test_check_room():
    p = init_play()
    assert check_room(p, 2, 1)
    assert not check_room(p, 4, 2)
    assert check_room(p, 0, 0)
    assert check_room(p, 3, 3)
    assert not check_room(p, 0, -1)
    print("Fonction check_room = Validée")

def test_get_value():
    p = {
        'n': 4,
        'nb_cases_libres': 8,
        'tiles': [0,1,2,0,6,0,0,1,12,0,0,24,2,0,0,48]
    }
    assert get_value(p, 2, 3) == 24
    assert get_value(p, 0, 1) == 1
    assert get_value(p, 1, 1) == 0
    assert not get_value(p, 0, 0) == 1
    assert get_value(p, 3, 3) == 48

    print("Fonction get_value = Validée")

def test_set_value():
    p = init_play()
    set_value(p, 0, 0, 1)
    assert p['tiles'][0] == 1 and p['nb_cases_libres'] == 15
    set_value(p, 1, 1, 6)
    assert p['tiles'][5] == 6 and p['nb_cases_libres'] == 14
    set_value(p, 1, 1, 2)
    set_value(p, 2, 3, 12)
    assert p['tiles'][11] == 12 and p['nb_cases_libres'] == 13
    set_value(p, 3, 3, 24)
    set_value(p, 0, 0, 0)
    assert p['tiles'][15] == 24 and p['nb_cases_libres'] == 13
    print("Fonction set_value = Vérifiée")

def test_is_room_empty():
    p = {
        'n': 4,
        'nb_cases_libres': 7,
        'tiles': [0,1,2,0,6,2,0,1,12,0,0,24,2,0,0,48]
    }
    assert is_room_empty(p, 0, 0)
    assert not is_room_empty(p, 0, 1)
    assert not is_room_empty(p, 3, 0)
    assert not is_room_empty(p, 3, 3)
    assert is_room_empty(p, 1, 2)
    print("Fonction is_room_empty = Validée")

def test_get_nb_empty_rooms():
    p = {
        'n': 4,
        'nb_cases_libres': 10,
        'tiles': [0,1,2,0,6,2,0,1,12,0,0,24,2,0,0,48]
    }
    assert get_nb_empty_rooms(p) == 7
    p['tiles'] = [0,1,0,0,6,0,0,0,12,0,0,24,2,0,0,48]
    assert not get_nb_empty_rooms(p) == 5
    assert get_nb_empty_rooms(p) == 10
    print("Fonction get_nb_empty_rooms = Validée")

def test_is_game_over():
    p = {
        'n': 4,
        'nb_cases_libres': 7,
        'tiles': [0,1,2,0,6,2,0,1,12,0,0,24,2,0,0,48]
    }
    assert not is_game_over(p)
    p = {
        'n': 4,
        'nb_cases_libres': 0,
        'tiles': [3,2,2,6,6,2,12,1,12,48,1,24,2,96,192,48]
    }
    assert is_game_over(p)
    print("Fonction is_game_over = Validée")

def test_get_score():
    p = {
        'n': 4,
        'nb_cases_libres': 0,
        'tiles': [3,2,3,6,6,2,12,1,12,48,1,24,2,96,192,48]
    }
    assert not get_score(p) == 16
    assert get_score(p) == 458
    p = {
        'n': 4,
        'nb_cases_libres': 0,
        'tiles': [48,2,24,6,6,2,12,1,12,48,1,24,2,96,192,48]
    }
    assert not get_score(p) == 458
    assert get_score(p) == 524
    print("Fonction get_score = Validée")












