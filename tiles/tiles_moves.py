﻿import sys
sys.path.append("../tiles")
import tiles_acces
from random import randint


def get_area_next_tiles(plateau, mode, val):
    """
    Retourne 1 ou 2 tuiles dont la position lig, col est tirée aléatoirement
    sur l'ensemble du plateau
    plateau = Dictionnaire contenant le jeu.
    mode = 'init' ou 'encours' selon le mode de jeu.
    """
    if mode == 'init':
        tiles = {
            'mode':mode,
            0: {'val':1, 'lig':randint(0,3), 'col':randint(0,3)},
            1: {'val':2, 'lig':randint(0,3), 'col':randint(0,3)},
            'check' : True
        }
        if tiles[0]['lig'] == tiles[1]['lig'] and tiles[0]['col'] == tiles[1]['col']:
            if tiles[0]['lig'] == 0:
                tiles[0]['lig'] += 1
            else:
                tiles[0]['lig'] -= 1
        return tiles
    else:
        tiles = {
            'mode':mode,
            0: {'val':val, 'lig':randint(0,3), 'col':randint(0,3) },
            'check': not tiles_acces.get_nb_empty_rooms(plateau) == 0,
        }
        if tiles['check'] == False:
            return tiles
        while not tiles_acces.is_room_empty(plateau, tiles[0]['lig'], tiles[0]['col']):
            tiles[0]['lig'] = randint(0,3)
            tiles[0]['col'] = randint(0,3)
        return tiles

def put_next_tiles(plateau, tiles):
    """
    Permet de placer une ou deux tuiles dans le plateau.
    plateau = plateau de jeu.
    tiles = dictionnaire contenant les tuiles à placer.
    """
    if tiles['check'] == False:
        return
    tiles_acces.set_value(plateau, tiles[0]['lig'], tiles[0]['col'], tiles[0]['val'])
    if tiles['mode'] == 'init':
        tiles_acces.set_value(plateau, tiles[1]['lig'], tiles[1]['col'], tiles[1]['val'])
    return

def line_pack(plateau, num_lig, debut, sens):
    """
    Tasse les tuiles d'une ligne dans un sens donné
    plateau = plateau de jeu.
    num_lig = Numéro de la ligne à tasser
    debut = A partir de quel indice on tasse
    sens = 1 pour gauche, 0 pour droite.
    """
    if sens == 1:
        fin = (4*(num_lig+1)) - 1
        ind = 4*num_lig+debut
        while ind < fin:
            if plateau['tiles'][ind] == 0:
                plateau['tiles'][ind] = plateau['tiles'][ind+1]
                plateau['tiles'][ind+1] = 0
            ind+=1
    else:
        fin = 4*num_lig
        ind = 4*num_lig+debut
        while ind > fin:
            if plateau['tiles'][ind] == 0:
                plateau['tiles'][ind] = plateau['tiles'][ind-1]
                plateau['tiles'][ind-1] = 0
            ind-=1
    return

def column_pack(plateau, num_col, debut, sens):
    """
    Tasse les tuiles d'une colonne dans un sens donné
    plateau = plateau de jeu.
    num_col = Numéro de la colonne à tasser
    debut = A partir de quel indice on tasse
    sens = 1 pour haut, 0 pour bas.
    """
    if sens == 1:
        fin = 12+num_col
        ind = 4*debut+num_col
        while ind < fin:
            if plateau['tiles'][ind] == 0:
                plateau['tiles'][ind] = plateau['tiles'][ind+4]
                plateau['tiles'][ind+4] = 0
            ind+=4
    else:
        fin = num_col
        ind = 4*debut+num_col
        while ind > fin:
            if plateau['tiles'][ind] == 0:
                plateau['tiles'][ind] = plateau['tiles'][ind-4]
                plateau['tiles'][ind-4] = 0
            ind-=4
    return

def line_move(plateau,num_lig, sens):
    """
    Déplacement des tuiles sur une ligne conformément au règles du jeu.
    plateau = plateau du jeu.
    num_lig = Numéro de la ligne à déplacer.
    sens = 1 pour gauche, 0 pour droite.
    """
    if sens == 1:
        fin = (4*(num_lig+1)) - 1
        ind = 4*num_lig
        Coup = False
        while ind<fin and not Coup:
            Case1 = plateau['tiles'][ind]
            Case2 = plateau['tiles'][ind+1]
            if Case1 == 0:
                line_pack(plateau, num_lig, 0, 1)
                Coup = True
            elif (Case1 == Case2 and Case1 != 1 and Case1 != 2) or (Case1 == 1 and Case2 == 2) or (Case1 == 2 and Case2 == 1):
                plateau['tiles'][ind] += Case2
                plateau['tiles'][ind+1] = 0
                line_pack(plateau, num_lig, 0, 1)
                Coup = True
            ind+=1
    else:
        fin = 4*num_lig
        ind = 4*(num_lig+1)-1
        Coup = False
        while ind>fin and not Coup:
            Case1 = plateau['tiles'][ind]
            Case2 = plateau['tiles'][ind-1]
            if Case1 == 0:
                line_pack(plateau, num_lig, 3, 0)
                Coup = True
            elif (Case1 == Case2 and Case1 != 1 and Case1 != 2) or (Case1 == 1 and Case2 == 2) or (Case1 == 2 and Case2 == 1):
                plateau['tiles'][ind] += Case2
                plateau['tiles'][ind-1] = 0
                line_pack(plateau, num_lig, 3, 0)
                Coup = True
            ind-=1
    return

def lines_move(plateau, sens):
    """
    Déplacement de toutes les lignes du plateau selon les règles du jeu.
    plateau = plateau du jeu.
    sens = 1 pour gauche, 0 pour droite.
    """
    if sens == 1:
        line_move(plateau, 0, 1)
        line_move(plateau, 1, 1)
        line_move(plateau, 2, 1)
        line_move(plateau, 3, 1)
    else:
        line_move(plateau, 0, 0)
        line_move(plateau, 1, 0)
        line_move(plateau, 2, 0)
        line_move(plateau, 3, 0)
    return

def column_move(plateau, num_col, sens):
    """
    Déplacement des tuiles sur une colonne conformément au règles du jeu.
    plateau = plateau du jeu.
    num_col = Numéro de la colonne à déplacer.
    sens = 1 pour haut, 0 pour bas.
    """
    if sens == 1:
        fin = 12+num_col
        ind = num_col
        Coup = False
        while ind<fin and not Coup :
            Case1 = plateau['tiles'][ind]
            Case2 = plateau['tiles'][ind+4]
            if Case1 == 0 :
                column_pack(plateau, num_col, 0, 1)
                Coup = True
            elif (Case1 == Case2 and Case1 != 1 and Case2 != 2) or (Case1 == 1 and Case2 == 2) or (Case1 == 2 and Case2 == 1):
                plateau['tiles'][ind] += Case2
                plateau['tiles'][ind+4] = 0
                column_pack(plateau, num_col, 0, 1)
                Coup = True
            ind+=4
    else:
        fin = num_col
        ind = 12+num_col
        Coup = False
        while ind>fin and not Coup:
            Case1 = plateau['tiles'][ind]
            Case2 = plateau['tiles'][ind-4]
            if Case1 == 0:
                column_pack(plateau, num_col, 3, 0)
            elif (Case1 == Case2 and Case1 != 1 and Case2 != 2) or (Case1 == 1 and Case2 == 2) or (Case1 == 2 and Case2 == 1):
                plateau['tiles'][ind] += Case2
                plateau['tiles'][ind-4] = 0
                column_pack(plateau, num_col, 3, 0)
                Coup = True
            ind-=4
    return

def columns_move(plateau, sens):
    """
    Déplacement de toutes les colonnes du plateau selon les règles du jeu.
    plateau = plateau du jeu.
    sens = 1 pour haut, 0 pour bas.
    """
    if sens == 1:
        column_move(plateau, 0, 1)
        column_move(plateau, 1, 1)
        column_move(plateau, 2, 1)
        column_move(plateau, 3, 1)
    else:
        column_move(plateau, 0, 0)
        column_move(plateau, 1, 0)
        column_move(plateau, 2, 0)
        column_move(plateau, 3, 0)
    return

def play_move(plateau, sens):
    """
    Déplace les tuiles du plateau dans un sens donné en appliquant les règles
    du jeu.
    plateau = plateau du jeu.
    sens =
        - 'h' pour Haut,
        - 'b' pour Bas,
        - 'g' pour Gauche,
        - 'd' pour Droite.
    """
    if sens == 'b' or sens == 'B':
        columns_move(plateau, 0)
    elif sens == 'h' or sens == 'H':
        columns_move(plateau, 1)
    elif sens == 'd' or sens == 'D':
        lines_move(plateau, 0)
    elif sens == 'g' or sens == 'G':
        lines_move(plateau, 1)
    return






