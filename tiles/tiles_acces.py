﻿def check_indice(plateau, ind):
    """
    Retourne True si l'indice correspond à un indice valide de case pour le plateau (entre 0 et n-1)
    """
    return int(ind) >= 0 and int(ind) <= plateau['n']-1

def check_room(plateau, lig, col):
    """
    Retourne True si (lig,col) est une case du plateau (lig et col sont des indices valides)
    """
    return check_indice(plateau, lig) and check_indice(plateau, col)

def get_value(plateau, lig, col):
    """
    Retourne la valeur de la case (lig,col).
    Erreur si (lig,col) non valide.
    """
    Verif = check_room(plateau, lig, col)
    assert Verif, "Erreur, Numéro de ligne ou de colonne invalide."
    return plateau['tiles'][4*lig + col]
    #4*lig + col permet de déterminer la position de la case.



def set_value(plateau, lig, col, val):
    """
    Affecte la valeur val dans la case (lig,col) du plateau.
    Erreur si (lig,col) n'est pas valide ou si val n'est pas supérieure ou égale à 0.
    Met aussi à jour le nombre de cases libres (sans tuile(s))
    """
    Verif = check_room(plateau, lig, col)
    assert Verif, "Erreur, Numéro de ligne ou de colonne invalide."
    assert val >= 0, "Erreur, La valeur est inférieure à 0."
    plateau['tiles'][4*lig+col] = val #Remplacement de la valeur.
    get_nb_empty_rooms(plateau) #Actualisation nb cases libres.
    return

def is_room_empty(plateau, lig, col):
    """
    Teste si une case du plateau est libre ou pas.
    Retourne True si la case est libre, False sinon.
    """
    return get_value(plateau, lig, col) == 0

def get_nb_empty_rooms(plateau):
    """
    Met à jour le dictionnaire plateau avec le nombre de cases libre(s) du plateau.
    Et renvoie le nombre de case(s) libre(s).
    """
    nblibres = 0
    i = 0
    while i < len(plateau['tiles']): #Regarde toutes les cases du tableau pour savoir
        if plateau['tiles'][i] == 0: #Lesquelles sont égales à 0.
            nblibres += 1
        i+=1
    plateau['nb_cases_libres'] = nblibres #Remplace la valeur du nb cases libres
    return nblibres


