def get_user_move():
    """
    Saisie et retourne le coup joue par le joueur parmi les choix.
    Retourne le choix de l'utilisateur (en minuscule) :
        - 'h' pour Haut,
        - 'b' pour Bas,
        - 'g' pour Gauche,
        - 'd' pour Droite,
        - 'm' pour Menu Principal.
    """
    print("Que voulez-vous faire ?\n- 'h' pour un coup vers le haut\n- 'b' pour un coup vers le bas\n- 'd' pour un coup vers la droite\n- 'g' pour un coup vers la gauche\n- 'm' pour le menu principal.")
    boucle = True
    while boucle:
        choix = input()
        if choix == 'h' or choix == 'H':
            return 'h'
        elif choix == 'b' or choix == 'B':
            return 'b'
        elif choix == 'd' or choix == 'D':
            return 'd'
        elif choix == 'g' or choix == 'G':
            return 'g'
        elif choix == 'm' or choix == 'M':
            return 'm'

def get_user_menu(partie):
    """
    Saisie et retourne le choix du joueur dans le menu principal:
        - Parametre partie : Partie de jeu en cours ou None sinon.
    Return Choix de l'utilisateur (en majuscule) :
        - 'N' : Nouvelle Partie.
        - 'L' : Charger une partie.
        - 'S' : Sauvegarder la partie en cours (si param partie est une partie en cours).
        - 'C' : Reprendre la partie en cours (si param partie est une partie en cours).
        - 'Q' : Quitter le jeu.
    """
    print("Menu Principal\nQue voulez-vous faire ?\n- 'N' : Nouvelle Partie.\n- 'L' : Charger une partie.\n- 'S' : Sauvegarder la partie en cours (si param partie est une partie en cours).\n- 'C' : Reprendre la partie en cours (si param partie est une partie en cours).\n- 'Q' : Quitter le jeu.")
    boucle = True
    while boucle :
        choix = input()
        if choix == 'n' or choix == 'N':
            return 'N'
        elif choix == 'l' or choix == 'L':
            return 'L'
        elif (choix == 's' or choix == 'S') and partie is not None:
            return 'S'
        elif (choix == 'c' or choix == 'C') and partie is not None:
            return 'C'
        elif choix == 'q' or choix == 'Q':
            return 'Q'




