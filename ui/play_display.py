﻿def affichage_simple(plateau):
    """
    Affichage du plateau 4*4 sans délimitation des cases.
    """
    i = 0
    while i < 4:
        ligne = ''
        j = 0
        while j < 4: #On effectue une concaténation pour toute la ligne.
            case = str(plateau['tiles'][4*i+j])
            if len(case) == 1 : #Selon la taille du nombre, on met plus ou moins d'espaces
                ligne += '   ' + case + '   '
            elif len(case) == 2:
                ligne += '  ' + case + '   '
            elif len(case) == 3:
                ligne += '  ' + case + '  '
            elif len(case) == 4:
                ligne += ' ' + case + '  '
            else:
                ligne += ' ' + case + ' '
            j+=1

        print(ligne)
        i+=1
    return

def affichage_moyen(plateau):
    """
    Affichage du plateau avec délimitation textuelle des cases
    par le symbole '*'.
    """
    i = 0
    while i < 4:
        ligne = '*'
        j = 0
        while j < 4: #On effectue une concaténation pour toute la ligne.
            case = str(plateau['tiles'][4*i+j])
            if len(case) == 1 : #Selon la taille du nombre, on met plus ou moins d'espaces
                ligne += '   ' + case + '   *'
            elif len(case) == 2:
                ligne += '  ' + case + '   *'
            elif len(case) == 3:
                ligne += '  ' + case + '  *'
            elif len(case) == 4:
                ligne += ' ' + case + '  *'
            else:
                ligne += ' ' + case + ' *'
            j+=1

        print(16*'* ' + '*')
        print('*       *       *       *       *')
        print(ligne)
        print('*       *       *       *       *')
        i+=1
    print(16*'* ' + '*') #Print la dernière ligne
    return
