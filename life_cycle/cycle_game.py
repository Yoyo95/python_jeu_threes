﻿import sys
sys.path.append("../ui")
import play_display
import user_entries
sys.path.append("../tiles")
import tiles_acces
import tiles_moves
from random import randint
import json

def init_play():

    """
    Retourne un plateau correspondant à une nouvelle partie.
    Une nouvelle partie est un dictionnaire avec les clefs de valeurs suivantes :
    - 'n' : vaut 4
    - 'nb_cases_libres' : 16 au départ
    - 'tiles' : tableau de 4*4 cases initialisées à 0
    """
    plateau = {
        'n': 4,
        'nb_cases_libres': 16,
        'tiles': [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    }
    return plateau

def is_game_over(plateau):
    """
    Retourne True si la partie est terminée, False sinon.
    """
    return plateau["nb_cases_libres"] == 0 # Si il n'y a plus de cases libres,
                                           # La partie est terminée.

def get_score(plateau):
    """
    Retourne le score du plateau.
    """
    score = 0
    i = 0
    while i < len(plateau['tiles']): #Additionne toutes les cases du plateau pour avoir le score.
        score += plateau['tiles'][i]
        i+=1
    return score

def create_new_play():
    """
    Créer et retourner une nouvelle partie.
    """
    p = init_play()
    t = tiles_moves.get_area_next_tiles(p, 'init', 1)
    tiles_moves.put_next_tiles(p, t)
    partie = {
        'plateau': p,
        'next_tile': None,
        'score': get_score(p)
    }
    return partie

def cycle_play(partie):
    """
    Permet de jouer à Threes :
        - Paramètre partie : Partie de jeu en cours ou None sinon.
        - Return True si la partie est terminée, False si menu demandé

    Séquencement des actions pour cette fonction :
        - I/ Afficher le plateau de jeu.
        - II/ Affiche la prochaine tuile pour informer le joueur.
        - III/ Saisir le mouvement proposé par le joueur; 2 cas possibles :
            - 1/ Jouer le coup du joueur courant, mettre à jour le score et revenir au point I.
            - 2/ Retourner False si menu demandé.
        - IV/ Si la partie terminée, return True.
    """
    if partie is None :
        partie = create_new_play()
    # I/
    tour = True
    while tour:
        play_display.affichage_moyen(partie['plateau'])
        print("Score = " + str(partie['score']))
        # II/
        val_next_tile = randint(1,3)
        print("La prochaine tuile sera un " + str(val_next_tile) +".\n")
        # III/
        move = user_entries.get_user_move()
        if move == 'm':
            # III/ 2/
            tour = False
        # III/ 1/
        tiles_moves.play_move(partie['plateau'], move)
        partie['next_tile'] = tiles_moves.get_area_next_tiles(partie['plateau'], 'encours', val_next_tile)
        if partie['next_tile']['check'] == True:
            tiles_moves.put_next_tiles(partie['plateau'], partie['next_tile'])
            tiles_acces.get_nb_empty_rooms(partie['plateau'])
            partie['score'] = get_score(partie['plateau'])
        else:
            tour = False
    return is_game_over(partie['plateau'])

def save_game(partie):
    """
    Sauvegarde une partie dans le fichier game_saved.json
    """
    fich = open("../save/game_saved.json", "w")
    json.dump(partie, fich, indent=4)
    fich.close()
    return

def restore_game():
    """
    Restaure et retourne une partie sauvegardée dans le fichier
    "game_saved.json" ou retourne une nouvelle partie si
    aucune partie n'est sauvegardée.
    """
    fich = open("../save/game_saved.json","rt")
    dic = fich.read()
    fich.close()
    if dic == '' :
        game = create_new_play()
    else:
        game = json.loads(dic)
    return game

def threes():
    """
    Permet d'enchainer les parties au jeu Threes, de reprendre une partie
    sauvegardée et de sauvegarder une partie en cours.
    """
    jeu = True
    choix = user_entries.get_user_menu(None)
    if choix == 'N':
        partie = create_new_play()
        terminee = cycle_play(partie)
        if terminee == True:
            print("\nFin de la partie ! Votre score est de " + str(partie['score'])+'\n\n')
            partie = None
    elif choix == 'L':
        partie = restore_game()
    elif choix == 'Q':
        jeu = False
    while jeu:
        choix = user_entries.get_user_menu(partie)
        if choix == 'N':
            partie = create_new_play()
            terminee = cycle_play(partie)
            if terminee == True:
                print("\nFin de la partie ! Votre score est de " + str(partie['score'])+"\n\n")
                partie = None
        elif choix == 'L':
            partie = restore_game()
        elif choix == 'S':
            save_game(partie)
        elif choix == 'C':
            terminee = cycle_play(partie)
            if terminee == True:
                print("\nFin de la partie ! Votre score est de " + str(partie['score'])+'\n\n')
                partie = None
        elif choix == 'Q':
            jeu = False
    return

threes()


















































